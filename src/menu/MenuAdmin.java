package menu;

import data.DataJawaban;
import data.DataPendaftar;
import data.DataSoal;
import java.awt.GridLayout;


import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import soal.TestCpns;

public class MenuAdmin extends Menu{
    
    private int kesempatan_login = projekakhir.ProjekAkhir.getKesempatan_login();
    Menu menuAwal;
    DataPendaftar dataAkun = new DataPendaftar();
    DataSoal dataSoal = new DataSoal();
    DataJawaban dataJawaban = new DataJawaban();
    
    public MenuAdmin() {
        if ( kesempatan_login < 1 ) {
            JOptionPane.showMessageDialog(null, "Anda tidak dapat login sebagai admin","Blocked", 0); 
            menuAwal = new Menu();
            menuAwal.tampilkanMenu();
            
        } else {
            login();
        }
    }
    
    boolean checkLogin(String id, String pass){
        if( id.equals("admin") && pass.equals("admin") ){
            return true;
        } else {
            return false;
        }
    }
    
    
    void login(){
        
        boolean loginLoop = true;
        while ( loginLoop ){	
            JPanel loginPanel = new JPanel(new GridLayout(3, 2,10,5));
            JTextField id = new JTextField();
            JPasswordField pass = new JPasswordField();

            loginPanel.add(new JLabel("Kesempatan login: "));
            loginPanel.add(new JLabel( Integer.toString(kesempatan_login) ));
            loginPanel.add(new JLabel("Username: "));
            loginPanel.add(id);
            loginPanel.add(new JLabel("Password: "));
            loginPanel.add(pass);
            
            int login = JOptionPane.showConfirmDialog(null, loginPanel, "Login", 2, 3);
            if ( login == JOptionPane.OK_OPTION ){
                
                if( checkLogin( id.getText(), pass.getText() ) == true ){
                    projekakhir.ProjekAkhir.setKesempatan_login(kesempatan_login);
                    loginLoop = false;
                    this.tampilkanMenu();
                    
                }else {
                    kesempatan_login -= 1;
                    JOptionPane.showMessageDialog(null, "Username / Password anda salah","Pesan Login", 2); 
                }

                if( kesempatan_login == 0 ){
                    loginLoop = false;
                    projekakhir.ProjekAkhir.setKesempatan_login(kesempatan_login);
                    JOptionPane.showMessageDialog(null, "Anda tidak dapat login sebagai admin","Blocked", 0); 
                    menuAwal = new Menu();
                    menuAwal.tampilkanMenu();
                }
            }else if ( login == JOptionPane.CANCEL_OPTION || login == JOptionPane.DEFAULT_OPTION ){
                
                projekakhir.ProjekAkhir.setKesempatan_login(kesempatan_login);
                loginLoop = false;
                menuAwal = new Menu();
                menuAwal.tampilkanMenu();
            }
	}
    }
    
    @Override
    public void tampilkanMenu(){
        JPanel adminPanel = new JPanel(new GridLayout(6, 1,10,5));
        adminPanel.add(new JLabel("1. Manajemem Soal Tes"));
        adminPanel.add(new JLabel("2. Manajemem Akun Pendaftar"));
        adminPanel.add(new JLabel("3. Seleksi Test CPNS 2020"));
        adminPanel.add(new JLabel("0. Kembali ke menu awal"));
        adminPanel.add(new JLabel(""));
        adminPanel.add(new JLabel("Masukan Pilihan:"));
        
        boolean loop = true;
        while(loop){
            try{
                Object[] options = { "1.", "2.", "3.", "0."};

            
                String opsi = (String)JOptionPane.showInputDialog(null, adminPanel, "Menu Admin", JOptionPane.QUESTION_MESSAGE, null, options, "");

                switch (opsi) {
                    case "1.":
                        loop = false;
                        menuManajemenSoal();
                        break;

                    case "2.":
                        loop = false;
                        menuManajemenAkun();
                        break;

                    case "3.":
                        loop = false;
                        if ( TestCpns.isBukaTes() == false ){
                            if ( !DataSoal.getArraySoal().isEmpty() && 
                                    !DataPendaftar.getArrayPendaftar().isEmpty() ){
                                JPanel bukaTesPanel = new JPanel(new GridLayout(2, 1,10,5));
                                bukaTesPanel.add(new JLabel("Tes CPNS 2020 Belum Dibuka."));
                                bukaTesPanel.add(new JLabel("Buka Tes CPNS?"));
                                int mulaiTes = JOptionPane.showConfirmDialog(null, bukaTesPanel, 
                                        "TES CPNS 2020", 
                                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                                if ( mulaiTes == JOptionPane.OK_OPTION ){
                                    TestCpns.setBukaTes(true);
                                    JOptionPane.showMessageDialog(null, "TEST CPNS 2020 TELAH DIBUKA",
                                            "TES CPNS 2020", JOptionPane.INFORMATION_MESSAGE); 

                                }else if ( mulaiTes == JOptionPane.NO_OPTION || mulaiTes == JOptionPane.DEFAULT_OPTION ){
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Soal Tes / Pendaftar Tidak ada",
                                        "TES CPNS 2020", JOptionPane.INFORMATION_MESSAGE); 
                            }
               
                        } else {
                            dataJawaban.lihatData();
                        }
                        tampilkanMenu();
                        break;

                    case "0.":
                        loop = false;
                        menuAwal = new Menu();
                        menuAwal.tampilkanMenu();
                        break;

                }
            } catch(NullPointerException e){
                JOptionPane.showMessageDialog(null, "Pilihan Tidak ada","ERROR", 0); 

            }
            
        }
    }
    
    void menuManajemenSoal(){
        JPanel adminPanel = new JPanel(new GridLayout(7, 1,10,5));
        adminPanel.add(new JLabel("1. Tambah Soal Tes"));
        adminPanel.add(new JLabel("2. Lihat Soal Tes"));
        adminPanel.add(new JLabel("3. Ubah Soal Tes"));
        adminPanel.add(new JLabel("4. Hapus Soal Tes"));
        adminPanel.add(new JLabel("0. Kembali ke menu awal"));
        adminPanel.add(new JLabel(""));
        adminPanel.add(new JLabel("Masukan Pilihan:"));
        
        
        boolean loop = true;
        while(loop){
            try{
                Object[] options = { "1.", "2.", "3.", "4.", "0."};

            
                String opsi = (String)JOptionPane.showInputDialog(null, adminPanel, 
                        "Manajemem Soal Tes", JOptionPane.QUESTION_MESSAGE, null, options, "");

                switch (opsi) {
                    case "1.":
                        loop = false;
                        if ( TestCpns.isBukaTes() == true ){
                            JOptionPane.showMessageDialog(null, 
                                    "Soal Tidak Dapat Diubah Setelah Pembukaan Sesi Test CPNS", 
                                    "Informasi", JOptionPane.INFORMATION_MESSAGE); 
                        } else {
                            dataSoal.tambahData();
                        }
                        menuManajemenSoal();
                        break;

                    case "2.":
                        loop = false;
                        dataSoal.lihatData();
                        menuManajemenSoal();
                        break;

                    case "3.":
                        loop = false;
                        if ( TestCpns.isBukaTes() == true ){
                            JOptionPane.showMessageDialog(null, 
                                    "Soal Tidak Dapat Diubah Setelah Pembukaan Sesi Test CPNS", 
                                    "Informasi", JOptionPane.INFORMATION_MESSAGE); 
                        } else {
                            dataSoal.updateData();
                        }
                        menuManajemenSoal();
                        break;

                    case "4.":
                        loop = false;
                        if ( TestCpns.isBukaTes() == true ){
                            JOptionPane.showMessageDialog(null, 
                                    "Soal Tidak Dapat Diubah Setelah Pembukaan Sesi Test CPNS", 
                                    "Informasi", JOptionPane.INFORMATION_MESSAGE); 
                        } else {
                            dataSoal.hapusData();
                        }
                        menuManajemenSoal();
                        break;

                    case "0.":
                        loop = false;
                        tampilkanMenu();
                        break;

                }
            } catch(NullPointerException e){
                JOptionPane.showMessageDialog(null, "Pilihan Tidak ada","ERROR", 0); 

            }
            
        }
    }
    
    void menuManajemenAkun(){
        JPanel adminPanel = new JPanel(new GridLayout(6, 1,10,5));
        adminPanel.add(new JLabel("1. Lihat Data Akun"));
        adminPanel.add(new JLabel("2. Ubah Data Akun"));
        adminPanel.add(new JLabel("3. Hapus Akun"));
        adminPanel.add(new JLabel("0. Kembali ke menu awal"));
        adminPanel.add(new JLabel(""));
        adminPanel.add(new JLabel("Masukan Pilihan:"));
        
        boolean loop = true;
        while(loop){
            try{
                Object[] options = { "1.", "2.", "3.", "0."};

            
                String opsi = (String)JOptionPane.showInputDialog(null, adminPanel, 
                        "Manajemem Akun Pendaftar", JOptionPane.QUESTION_MESSAGE, null, options, "");

                switch (opsi) {
                    case "1.":
                        loop = false;
                        dataAkun.lihatData();
                        menuManajemenAkun();
                        break;

                    case "2.":
                        loop = false;
                         dataAkun.updateData();
                        menuManajemenAkun();
                        break;

                    case "3.":
                        loop = false;
                         dataAkun.hapusData();
                        menuManajemenAkun();
                        break;

                    case "0.":
                        loop = false;
                        tampilkanMenu();
                        break;

                }
            } catch(NullPointerException e){
                JOptionPane.showMessageDialog(null, "Pilihan Tidak ada","ERROR", 0); 

            }
            
        }
    }
    
}
