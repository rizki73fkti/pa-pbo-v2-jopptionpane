package menu;
import data.DataPendaftar;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Menu{
    
    public void tampilkanMenu(){
        boolean loop = true;
        JPanel menuPanel = new JPanel(new GridLayout(5, 1,10,5));
        menuPanel.add(new JLabel("1. Admin"));
        menuPanel.add(new JLabel("2. Pendaftar"));
        menuPanel.add(new JLabel("3. EXIT"));
        menuPanel.add(new JLabel(""));
        menuPanel.add(new JLabel("Pilihan menu:"));
        
        while(loop){
            try{
                Object[] options = { "Admin", "Pendaftar", "Exit"};
                
                String opsi = (String)JOptionPane.showInputDialog(null, menuPanel, 
                        "Menu Awal" , JOptionPane.QUESTION_MESSAGE, null, options, "");

                switch (opsi) {
                    case "Admin":
                        loop = false;
                        MenuAdmin admin = new MenuAdmin();
                        break;
                        
                    case "Pendaftar":
                        loop = false;
                        MenuPendaftar pendaftar= new MenuPendaftar();
                        break;
                        
                    case "Exit":
                        loop = false;
                        JOptionPane.showMessageDialog(null, "Terima Kasih", "Exit Program", JOptionPane.INFORMATION_MESSAGE);
                        break;
                        
                }

            } catch(NullPointerException e){
                JOptionPane.showMessageDialog(null, "Pilihan Tidak ada","ERROR", JOptionPane.ERROR_MESSAGE); 

            }
        }
    }
    
}