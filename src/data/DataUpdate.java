package data;

abstract class DataUpdate extends DataManagement{
    @Override
    abstract void tambahData();
    @Override
    abstract void lihatData();
    @Override
    abstract void hapusData();
    abstract void updateData();
    
}
