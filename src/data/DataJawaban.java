package data;

import java.awt.GridLayout;
import soal.Jawaban;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import soal.TestCpns;


public class DataJawaban extends DataManagement{
    
    private static ArrayList<Jawaban> arrayJawaban = new ArrayList<>();
    private static float nilai = 0;
    
    
    DataSoal dataSoal = new DataSoal();
    
    
    @Override
    public void tambahData() {
        addArrayJawaban();
    }
    
    public static void addArrayJawaban() {
        arrayJawaban.add( new Jawaban() );
    }
    
    
    public static ArrayList<Jawaban> getArrayJawaban() {
        return arrayJawaban;
    }
    
    public static Jawaban getArrayIndex(int i) {
        return arrayJawaban.get(i);
    }
    
    @Override
    public void lihatData() {
         if ( !DataPendaftar.getArrayPendaftar().isEmpty() && TestCpns.isBukaTes() == true){
            JPanel dataSeleksiPanel = new JPanel(new GridLayout( (DataPendaftar.getArrayPendaftar().size()*2) + 3, 3, 2, 5));
            dataSeleksiPanel.add(new JLabel("HASIL SELEKSI TEST CPNS 2020"));
            dataSeleksiPanel.add(new JLabel(""));
            dataSeleksiPanel.add(new JLabel(""));

            dataSeleksiPanel.add(new JLabel("No."));
            dataSeleksiPanel.add(new JLabel("NIK"));
            dataSeleksiPanel.add(new JLabel("Nama"));
       
            for (int i = 0; i < DataPendaftar.getArrayPendaftar().size()   ; i++){
                dataSeleksiPanel.add(new JLabel( Integer.toString( i+1 ) ));
                dataSeleksiPanel.add(new JLabel( Integer.toString ( DataPendaftar.getArrayIndex(i).getNik() ) ));
                dataSeleksiPanel.add(new JLabel( DataPendaftar.getArrayIndex(i).getNama() ));
                dataSeleksiPanel.add(new JLabel( "   Nilai : " + DataJawaban.getArrayIndex(i).getNilai() ));
                dataSeleksiPanel.add(new JLabel(""));
                dataSeleksiPanel.add(new JLabel(""));
            }

            boolean loop = true;
            while(loop){
                try{
                    Object[] options = { "1. Publikasi hasil seleksi", "2. Kembali ke menu admin "};

                    String opsi = (String)JOptionPane.showInputDialog( null, dataSeleksiPanel, 
                            "Seleksi Test CPNS 2020", JOptionPane.QUESTION_MESSAGE, null, options, "");

                    switch (opsi) {
                        case "1. Publikasi hasil seleksi":
                            loop = false;
                            TestCpns.setBukaSeleksi(true);
                            JOptionPane.showMessageDialog(null, "Hasil Seleksi telah dibuka", 
                                    "Hasil Seleksi", JOptionPane.INFORMATION_MESSAGE);
                            break;

                        case "2. Kembali ke menu admin ":
                            loop = false;
                            break;

                    }
                } catch(NullPointerException e){
                    JOptionPane.showMessageDialog(null, "Pilihan Tidak ada","ERROR", JOptionPane.ERROR_MESSAGE); 

                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Data Seleksi Kosong", "Hasil Seleksi", JOptionPane.INFORMATION_MESSAGE);
        }
                    
    }
    
    public static void updateData(int indeks, ArrayList<String> jawaban){
        
        getArrayIndex(indeks).setJawaban(jawaban);
        
        float nilai = 0;
        for ( int i = 0; i < DataSoal.getArraySoal().size(); i++){
            if ( getArrayIndex(indeks).getJawabanIndex(i).equalsIgnoreCase( DataSoal.getArrayIndex(i).getJawabanBenar() )){
                nilai += DataSoal.getArrayIndex(i).getNilai();
            }
        }
        getArrayIndex(indeks).setNilai(nilai);
        getArrayIndex(indeks).setTelahIkutTes(true);
    }
    
    @Override
    public void hapusData() {
        int indeks = DataPendaftar.getIndeks_hapusData();
        getArrayJawaban().remove(indeks);
        
    }
    
    
}
