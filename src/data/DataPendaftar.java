package data;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class DataPendaftar extends DataUpdate{
    
    private static ArrayList<Pendaftar> arrayPendaftar = new ArrayList<>();
    private static int indeks_hapusData;

    
    @Override
    void tambahData() {
    }
    
    public static void tambahData(
            String nama, int nik, int no_kk, 
            int tanggal, int bulan, int tahun, 
            String password, int kesempatan_ubah_data) {    
        
        
        arrayPendaftar.add( new Pendaftar(
                nama, nik,  no_kk, 
                tanggal,  bulan,  tahun, 
                password, kesempatan_ubah_data
                )
        
        );
        DataJawaban.addArrayJawaban();
    }

    
    public static ArrayList<Pendaftar> getArrayPendaftar() {
        return arrayPendaftar;
    }
    
    public static Pendaftar getArrayIndex(int i) {
        return arrayPendaftar.get(i);
    }
    
    public JPanel lihatAkunPanel(){
        JPanel dataAkunPanel = new JPanel(new GridLayout( getArrayPendaftar().size() + 1, 3, 2, 5));
        dataAkunPanel.add(new JLabel("No."));
        dataAkunPanel.add(new JLabel("NIK"));
        dataAkunPanel.add(new JLabel("Nama"));
        
        for (int i = 0; i < getArrayPendaftar().size()   ; i++){
            dataAkunPanel.add(new JLabel( Integer.toString( i+1 ) ));
            dataAkunPanel.add(new JLabel( Integer.toString( getArrayIndex(i).getNik() )));
            dataAkunPanel.add(new JLabel( getArrayIndex(i).getNama() ));
        }
        
        return dataAkunPanel;
        
    }

    @Override
    public void lihatData() {
        
        if ( !getArrayPendaftar().isEmpty() ){
            
            boolean loop = true;
            while(loop){
                try{
                    Object[] options = { "1. Lihat Data Detail", "2. Kembali ke menu admin"};

                    String opsi = (String)JOptionPane.showInputDialog(null, lihatAkunPanel(), "Data Pendaftar", JOptionPane.QUESTION_MESSAGE, null, options, "");

                    switch (opsi) {
                        case "1. Lihat Data Detail":
                            loop = false;
                            int indeks = cariData();
                            if( indeks != -2 && indeks != -1 ){                
                                lihatData(indeks);
                            } else if ( indeks != -2 ) {
                                JOptionPane.showMessageDialog(null, "Maaf Data yang dicari tidak ditemukan",
                                        "Data Pendaftar", JOptionPane.INFORMATION_MESSAGE);
                            }
                            break;

                        case "2. Kembali ke menu admin":
                            loop = false;
                            break;
                    }
                } catch(NullPointerException e){
                    JOptionPane.showMessageDialog(null, "Pilihan Tidak ada","ERROR", 0); 

                }
            }
            
        } else {
            JOptionPane.showMessageDialog(null, "Data Pendaftar Kosong", "Data Pendaftar", JOptionPane.INFORMATION_MESSAGE);
            
        }
    }
    
    public void lihatData(int indeks) {
        JPanel dataDetailPanel = new JPanel(new GridLayout( 5 , 2, 2, 5));
        dataDetailPanel.add(new JLabel("Detail Data Pendaftar"));
        dataDetailPanel.add(new JLabel(""));
        
        dataDetailPanel.add(new JLabel("Nama:"));
        dataDetailPanel.add(new JLabel( getArrayIndex(indeks).getNama() ));
        
        dataDetailPanel.add(new JLabel("NIK:"));
        dataDetailPanel.add(new JLabel( Integer.toString( getArrayIndex(indeks).getNik() ) ));
        
        dataDetailPanel.add(new JLabel("No. KK:"));
        dataDetailPanel.add(new JLabel( Integer.toString( getArrayIndex(indeks).getNoKk() ) ));
        
        dataDetailPanel.add(new JLabel("Tanggal Lahir:"));
        dataDetailPanel.add(new JLabel(  
         (Integer.toString ( getArrayIndex(indeks).getTanggal() ))  + " - "  + 
         (Integer.toString ( getArrayIndex(indeks).getBulan() ))    + " - "  + 
         (Integer.toString ( getArrayIndex(indeks).getTahun() )) ) );
        
        JOptionPane.showMessageDialog(null, dataDetailPanel, "Data Detail Pendaftar", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public int cariData(){
        int indeks = -1;
        try{
            String input = JOptionPane.showInputDialog(null, "Masukkan NIK pendaftar:", 
                    "Cari Data Pendaftar", JOptionPane.QUESTION_MESSAGE);
            
            
            if( input == null ){                
                indeks = -2;
                
            } else {
                int inputNik = Integer.parseInt(input);
                for (int i=0; i < getArrayPendaftar().size();i++){
                    if ( inputNik == getArrayIndex(i).getNik() ) {
                        indeks = i;
                        break;
                    }
                }
            }
        } catch  ( NumberFormatException e ){
           indeks = -1;
        }
        return indeks;
    }
    
    @Override
    public void updateData() {
        if ( getArrayPendaftar().isEmpty()){
            JOptionPane.showMessageDialog(null, "Data Pendaftar Kosong", "Cari Data Pendaftar", JOptionPane.INFORMATION_MESSAGE);
        } else {
            int indeks = cariData();
            if( indeks != -2 && indeks != -1 ){                
                JOptionPane.showMessageDialog(null, "Data " + getArrayIndex(indeks).getNama() + " ditemukan",
                                "Cari Data Pendaftar", JOptionPane.INFORMATION_MESSAGE);
                updateData(indeks);
            } else if ( indeks != -2 ) {
                JOptionPane.showMessageDialog(null, "Maaf Data yang dicari tidak ditemukan",
                                            "Data Pendaftar", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }
    
    public void updateData(int indeks) {
        JPanel updatePanel = new JPanel(new GridLayout(6, 2,10,5));
        JTextField nama= new JTextField();
        JTextField nik = new JTextField(); 
        JTextField noKk = new JTextField(); 
        JTextField tanggal = new JTextField(); 
        JTextField bulan = new JTextField();
        JTextField tahun = new JTextField();


        updatePanel.add(new JLabel("Nama Lengkap: "));
        updatePanel.add( nama );

        updatePanel.add(new JLabel("NIK: "));
        updatePanel.add( nik );
        
        updatePanel.add(new JLabel("No. KK: "));
        updatePanel.add( noKk );
        
        updatePanel.add(new JLabel("Tanggal Lahir: "));
        updatePanel.add( tanggal );
        
        updatePanel.add(new JLabel("Bulan Lahir: "));
        updatePanel.add( bulan );
        
        updatePanel.add(new JLabel("Tahun Lahir: "));
        updatePanel.add( tahun );
        try{
            int login = JOptionPane.showConfirmDialog(null, updatePanel, "Update Akun Pendaftar", 
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

            if ( login == JOptionPane.OK_OPTION ){
                if ( nama.getText().isEmpty()){
                        JOptionPane.showMessageDialog(null, "Nama tidak boleh kosong", "ERROR", JOptionPane.WARNING_MESSAGE); 
                } else {
                    int nikInt = Integer.parseInt( nik.getText() );
                    int noKkInt = Integer.parseInt( noKk.getText()); 
                    int tanggalInt = Integer.parseInt( tanggal.getText());
                    int bulanInt = Integer.parseInt( bulan.getText());
                    int tahunInt = Integer.parseInt( tahun.getText());

                    getArrayIndex(indeks).setNik( nikInt);
                    getArrayIndex(indeks).setNoKk( noKkInt);
                    getArrayIndex(indeks).setTanggal( tanggalInt);
                    getArrayIndex(indeks).setBulan( bulanInt);
                    getArrayIndex(indeks).setTahun( tahunInt);
                    getArrayIndex(indeks).setNama( nama.getText() );
                    getArrayIndex(indeks).setKesempatan_ubah_data(0);

                    JOptionPane.showMessageDialog(null, "Perubahan data sukses", "Update Akun Pendaftar", JOptionPane.INFORMATION_MESSAGE); 
                }
            }else if ( login == JOptionPane.CANCEL_OPTION || login == JOptionPane.DEFAULT_OPTION ){
            }
        } catch ( NumberFormatException e ){
            JOptionPane.showMessageDialog(null, 
                    "NIK / No. KK / Tanggal / Bulan / Tahun harus berupa angka", 
                    "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void hapusData() {
        if ( getArrayPendaftar().isEmpty()){
            JOptionPane.showMessageDialog(null, "Data Pendaftar Kosong", "Cari Data Pendaftar", JOptionPane.INFORMATION_MESSAGE);
        } else {
            int indeks = cariData();
            if( indeks != -2 && indeks != -1 ){         
                JOptionPane.showMessageDialog(null, "Data " + getArrayIndex(indeks).getNama() + " berhasil dihapus",
                                "Hapus Data Pendaftar", JOptionPane.INFORMATION_MESSAGE);
                getArrayPendaftar().remove(indeks);
                setIndeks_hapusData(indeks);
                DataJawaban dataJawaban = new DataJawaban();
                dataJawaban.hapusData();
                
            } else if ( indeks != -2 ) {
                JOptionPane.showMessageDialog(null, "Maaf Data yang dicari tidak ditemukan",
                                "Data Pendaftar", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        
    }

    public static int getIndeks_hapusData() {
        return indeks_hapusData;
    }

    public void setIndeks_hapusData(int indeks_hapusData) {
        DataPendaftar.indeks_hapusData = indeks_hapusData;
    }
    
    
    
}
