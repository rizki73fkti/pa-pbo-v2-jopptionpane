package soal;

import data.Button;
import data.DataSoal;
import data.DataJawaban;
import data.DataPendaftar;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TestCpns {
    private static boolean bukaTes = false;
    private static boolean bukaSeleksi = false;
    private static int index = 0;
    
    private static JLabel pertanyaan = new JLabel();
    private static JLabel jawabanNo1 = new JLabel();
    private static JLabel jawabanNo2 = new JLabel(); 
    private static JLabel jawabanNo3 = new JLabel();
    private static JLabel jawabanNo4 = new JLabel();
    private static JLabel jawabanNo5 = new JLabel();
    private static JLabel jawabanPendaftar = new JLabel();

    private static ArrayList<String> jawaban_sementara;
    
    public static void setBukaTes(boolean bukaTes) {
        TestCpns.bukaTes = bukaTes;
    }

    public static void setBukaSeleksi(boolean bukaSeleksi) {
        TestCpns.bukaSeleksi = bukaSeleksi;
    }

    public static boolean isBukaTes() {
        return bukaTes;
    }
    
    public static boolean isBukaSeleksi() {
        return bukaSeleksi;
    }

    
    public static void mulaiTes(int indeks){
        if ( DataSoal.getArraySoal().isEmpty() || TestCpns.isBukaTes() == false ){
            JOptionPane.showMessageDialog(null, "Soal Tes CPNS Belum tersedia saat ini", 
                        "TES CPNS 2020", JOptionPane.INFORMATION_MESSAGE);
            
        } else {
            
            JPanel testPanel = new JPanel(new GridLayout( 4 , 1, 2, 5));

            testPanel.add(new JLabel("Memulai Tes CPNS 2020"));
            testPanel.add(new JLabel("Jumlah Soal: " + 
                    Integer.toString( DataSoal.getArraySoal().size() )));
            testPanel.add(new JLabel("Anda Tidak Dapat Kembali Hingga Tes Selesai."));
            testPanel.add(new JLabel("Mulai tes? ( y / t ): "));
            
            int mulaiTes = JOptionPane.showConfirmDialog(null, testPanel, "TES CPNS 2020", 
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            
            if ( mulaiTes == JOptionPane.OK_OPTION ){
                jawabSoal(indeks);

            }else if ( mulaiTes == JOptionPane.NO_OPTION || mulaiTes == JOptionPane.DEFAULT_OPTION ){
            }
            
        }
    }
    
    
    public static void jawabSoal(int indeks){
        jawaban_sementara = new ArrayList<>();
        
        for (int i = 0; i < DataSoal.getArraySoal().size()   ; i++){
            JPanel testPanel = new JPanel(new GridLayout(7, 2, 5,5));
            
            testPanel.add( pertanyaan );
            testPanel.add(new JLabel(""));
            
            testPanel.add(new JLabel("Nilai per soal: " + DataSoal.getArrayIndex(0).getNilai()  ));
            testPanel.add(new JLabel(""));

            testPanel.add( jawabanNo1 );
            testPanel.add(new JLabel(""));

            testPanel.add( jawabanNo2 );
            testPanel.add(new JLabel(""));

            testPanel.add( jawabanNo3 );
            testPanel.add(new JLabel(""));

            testPanel.add( jawabanNo4 );
            testPanel.add(new JLabel(""));

            testPanel.add( jawabanNo5 );
            testPanel.add(new JLabel(""));
            
            pertanyaan.setText(Integer.toString(i+1) +". "+ DataSoal.getArrayIndex(i).getQuestion() );

            jawabanNo1.setText( "    a. " + DataSoal.getArrayIndex(i).getJawaban_1() ) ;
            jawabanNo2.setText( "    b. " + DataSoal.getArrayIndex(i).getJawaban_2() ) ;

            jawabanNo3.setText( "    c. " + DataSoal.getArrayIndex(i).getJawaban_3() );
            jawabanNo4.setText( "    d. " + DataSoal.getArrayIndex(i).getJawaban_4() );
            jawabanNo5.setText( "    e. " + DataSoal.getArrayIndex(i).getJawaban_5() );
            
            Object[] options = { "a", "b", "c", "d", "e"};
            try{
                String input_jawaban = (String)JOptionPane.showInputDialog(null,testPanel, 
                        "TES CPNS 2020", JOptionPane.QUESTION_MESSAGE, null, options, "");

                jawaban_sementara.add(input_jawaban); 
            } catch(NullPointerException e){
            }
        }
          
       
        JPanel testPanel = new JPanel(new GridLayout(8, 2, 5,5));
        
        index = 0;
        
        testPanel.add( pertanyaan );
        testPanel.add(new JLabel(""));

        testPanel.add( jawabanNo1 );
        testPanel.add(new JLabel(""));

        testPanel.add( jawabanNo2 );
        testPanel.add(new JLabel(""));

        testPanel.add( jawabanNo3 );
        testPanel.add(new JLabel(""));

        testPanel.add( jawabanNo4 );
        testPanel.add(new JLabel(""));

        testPanel.add( jawabanNo5 );
        testPanel.add(new JLabel(""));

        testPanel.add( jawabanPendaftar );
        testPanel.add(new JLabel(""));

        testPanel.add( Button.previousTesSoal );
        testPanel.add( Button.nextTesSoal );
        
        pertanyaan.setText(Integer.toString(index+1) +". "+ DataSoal.getArrayIndex(index).getQuestion() );

        jawabanNo1.setText( "    a. " + DataSoal.getArrayIndex(index).getJawaban_1() ) ;
        jawabanNo2.setText( "    b. " + DataSoal.getArrayIndex(index).getJawaban_2() ) ;
        jawabanNo3.setText( "    c. " + DataSoal.getArrayIndex(index).getJawaban_3() );
        jawabanNo4.setText( "    d. " + DataSoal.getArrayIndex(index).getJawaban_4() );
        jawabanNo5.setText( "    e. " + DataSoal.getArrayIndex(index).getJawaban_5() );
        jawabanPendaftar.setText( "    Jawaban anda: " + jawaban_sementara.get(index) );
        boolean loop = true;
        while(loop){
            try{
                Object[] options = { "1. Selesai Tes", "2. Ubah Jawaban"};
            
                String opsi = (String)JOptionPane.showInputDialog( 
                        null, testPanel, 
                        "Selamat Datang, " + DataPendaftar.getArrayIndex(indeks).getNama(), 
                        JOptionPane.QUESTION_MESSAGE, null, options, "");

                switch (opsi) {
                    case "1. Selesai Tes":
                        JPanel konfirmSelesaiPanel = new JPanel(new GridLayout(3, 1, 5,5));

                        konfirmSelesaiPanel.add(new JLabel("       KONFiRMASI SELESAI TES"));
                        konfirmSelesaiPanel.add(new JLabel("Anda yakin ingin menyelesaikan tes ini?"));
                        
                        int selesaiTes = JOptionPane.showConfirmDialog(null, konfirmSelesaiPanel, 
                                "Konfirmasi Selesai Tes CPNS 2020", 2, 3);
                        
                        if ( selesaiTes == JOptionPane.OK_OPTION ){

                            DataJawaban.updateData(indeks, jawaban_sementara);
                            JPanel selesaiPanel = new JPanel(new GridLayout(2, 1, 5,5));

                            selesaiPanel.add(new JLabel("          TES TELAH SELESAI"));
                            selesaiPanel.add(new JLabel("Semoga anda lolos ke tahap berikutnya"));
                            JOptionPane.showMessageDialog(null, selesaiPanel,"TES CPNS 2020", 
                                    JOptionPane.INFORMATION_MESSAGE); 

                            loop = false;

                        }else if ( selesaiTes == JOptionPane.CANCEL_OPTION || 
                                selesaiTes == JOptionPane.DEFAULT_OPTION ){
                        }

                        break;

                    case "2. Ubah Jawaban":
                        loop = false;
                        jawabSoal(indeks);
                        break;

                }
            } catch(NullPointerException e){
                JOptionPane.showMessageDialog(null, "Jawaban Tidak dapat kosong","ERROR", JOptionPane.ERROR_MESSAGE); 

            }

        }
    }
    
    public static void lihatHasilSeleksi(int indeks) {
        if ( bukaSeleksi == true){
            JPanel hasilSeleksiPanel = new JPanel(new GridLayout( 8 , 1, 2, 5));

            hasilSeleksiPanel.add(new JLabel("Nama: " +
                    DataPendaftar.getArrayIndex(indeks).getNama() ));

            hasilSeleksiPanel.add(new JLabel("NIK: " +
                    DataPendaftar.getArrayIndex(indeks).getNik() ));

            hasilSeleksiPanel.add(new JLabel("No. KK: " + DataPendaftar.getArrayIndex(indeks).getNoKk() ));

            hasilSeleksiPanel.add(new JLabel("Tanggal Lahir: " +
                DataPendaftar.getArrayIndex(indeks).getTanggal()  + " - "  + 
                DataPendaftar.getArrayIndex(indeks).getBulan()    + " - "  + 
                DataPendaftar.getArrayIndex(indeks).getTahun() ));
            
            float nilai = DataJawaban.getArrayIndex(indeks).getNilai();
            hasilSeleksiPanel.add(new JLabel("NILAI ANDA: " +
                    Float.toString(nilai) ));
            hasilSeleksiPanel.add(new JLabel("" ));
            
            if ( nilai >= 80.0 ){
                hasilSeleksiPanel.add(new JLabel("         SELAMAT ANDA LOLOS TAHAP TES CPNS 2020!         " ));
                hasilSeleksiPanel.add(new JLabel("Silahkan menunggu info berikutnya untuk tahap selanjutnya" ));
            } else {
                hasilSeleksiPanel.add(new JLabel("MOHON MAAF ANDA TIDAK LULUS TAHAP TES   " ));
                hasilSeleksiPanel.add(new JLabel("" ));
                
            }
            
            JOptionPane.showMessageDialog(null, hasilSeleksiPanel, "HASIL SELEKSI TES CPNS 2020", JOptionPane.INFORMATION_MESSAGE);
            
            
        } else {
            JPanel hasilSeleksiPanel = new JPanel(new GridLayout( 2 , 1, 2, 5));

            hasilSeleksiPanel.add(new JLabel(" Hasil Seleksi Tes CPNS belum keluar. "));
            hasilSeleksiPanel.add(new JLabel("Hasil keluar pada tanggal 1 Juli 2020."));
            JOptionPane.showMessageDialog(null, hasilSeleksiPanel, 
                        "HASIL SELEKSI TES CPNS 2020", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    
    
    //SETTER JLabel
    public static void setIndex(int index) {
        TestCpns.index = index;
    }

    public static void setPertanyaan( String pertanyaan ) {
        TestCpns.pertanyaan.setText(pertanyaan);
    }

    public static void setJawabanNo1(String jawabanNo1) {
        TestCpns.jawabanNo1.setText( jawabanNo1 );
    }

    public static void setJawabanNo2( String jawabanNo2) {
        TestCpns.jawabanNo2.setText( jawabanNo2 );
    }

    public static void setJawabanNo3(String jawabanNo3) {
        TestCpns.jawabanNo3.setText(  jawabanNo3 );
    }

    public static void setJawabanNo4(String jawabanNo4) {
        TestCpns.jawabanNo4.setText( jawabanNo4);
    }

    public static void setJawabanNo5(String jawabanNo5) {
        TestCpns.jawabanNo5.setText( jawabanNo5 );
    }

    public static void setJawabanPendaftar(String jawabanPendaftar) {
        TestCpns.jawabanPendaftar.setText( jawabanPendaftar );
    }

    public static void setJawaban_sementara(ArrayList<String> jawaban_sementara) {
        TestCpns.jawaban_sementara = jawaban_sementara;
    }
    
    
    //GETTER JLabel
    public static int getIndex() {
        return index;
    }

    public static ArrayList<String> getJawaban_sementara() {
        return jawaban_sementara;
    }
    
    public static String getArrayIndex(int i) {
        return jawaban_sementara.get(i);
    }
    
    
    
}
